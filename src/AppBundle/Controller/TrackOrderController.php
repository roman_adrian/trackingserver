<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TrackingOrderEntity;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use AppBundle\Entity\RequestData;
use AppBundle\Service\FetchTrackingRequestService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TrackOrderController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/order/{orderId}")
     *
     * @param string $orderId
     * @param FetchTrackingRequestService $fetchTracking
     * @return TrackingOrderEntity
     */
    public function getAction(string $orderId, FetchTrackingRequestService $fetchTracking): ?TrackingOrderEntity
    {
        /** @var TrackingOrderEntity $data */
        $data = $fetchTracking->getTrackingRequest($orderId);

        if (!$data){
            throw new BadRequestHttpException(sprintf('The package with the order \'%s\' was not found.', $orderId));
        }

        return $data;
    }
}