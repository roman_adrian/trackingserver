

create database trackingdb;
use trackingdb;
create user 'trackingDbUser'@'localhost' identified by '7MwJT~q#$?mEJ9fr';
GRANT ALL PRIVILEGES ON *.* TO 'trackingDbUser'@'localhost';

insert into tracking_order_entity (tracking_id,shipping_date) values ('1',now());
insert into tracking_order_entity (tracking_id,shipping_date) values ('2',now());
insert into tracking_order_entity (tracking_id,shipping_date) values ('3',now());
insert into tracking_order_entity (tracking_id,shipping_date) values ('4',now());
