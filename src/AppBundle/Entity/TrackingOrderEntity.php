<?php

namespace AppBundle\Entity;

use DateTime;

/**
 * TrackingOrderEntity
 */
class TrackingOrderEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $trackingId;

    /**
     * @var DateTime
     */
    private $shippingDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * Set trackingId
     *
     * @param string $trackingId
     *
     * @return TrackingOrderEntity
     */
    public function setTrackingId(String $trackingId): TrackingOrderEntity
    {
        $this->trackingId = $trackingId;

        return $this;
    }

    /**
     * Get trackingId
     *
     * @return string
     */
    public function getTrackingId(): String
    {
        return $this->trackingId;
    }

    /**
     * Set shippingDate
     *
     * @param DateTime $shippingDate
     *
     * @return TrackingOrderEntity
     */
    public function setShippingDate(DateTime $shippingDate):TrackingOrderEntity
    {
        $this->shippingDate = $shippingDate;

        return $this;
    }

    /**
     * Get shippingDate
     *
     * @return DateTime
     */
    public function getShippingDate(): DateTime
    {
        return $this->shippingDate;
    }
}

