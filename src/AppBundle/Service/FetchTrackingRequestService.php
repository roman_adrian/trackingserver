<?php

namespace AppBundle\Service;

use AppBundle\Entity\TrackingOrderEntity;

/**
 * FetchTrackingRequestService
 */
class FetchTrackingRequestService
{
    /**
     * @var FetchTrackingRequestInterface
     */
    private $dataSource;

    /**
     * FetchTrackingRequestService constructor.
     * @param string|null $source
     * @param FetchTrackingRequestFromCSV $sourceFromCsv
     * @param FetchTrackingRequestFromDB $sourceFromDB
     */
    public function __construct(string $source=null, FetchTrackingRequestFromCSV $sourceFromCsv, FetchTrackingRequestFromDB $sourceFromDB){
        if ($source=='csv'){
            $this->dataSource = $sourceFromCsv;
        } else {
            $this->dataSource = $sourceFromDB;
        }
    }
    /**
     * fetch a tracking order from the database
     *
     * @param string $trackingRequest
     * @return TrackingOrderEntity|Null
     */
    public function getTrackingRequest (string $trackingRequest):?TrackingOrderEntity
    {
        /** @var $entity TrackingOrderEntity */
        $entity = $this->dataSource->getTrackingRequest($trackingRequest);
        return $entity;
        /*
        if (!$entity){
            return null;
        }

        return $this->entityToString($entity);*/
    }

    /**
     * @param TrackingOrderEntity $entity
     * @return String|null
     */
    private function entityToString (TrackingOrderEntity $entity):?String
    {
        /** @var array $data */
        $data = array(
            "trackingID"=>$entity->getTrackingId(),
            "deliveryDate"=>$entity->getShippingDate()->format('Y-m-d')
        );

        return json_encode($data);
    }
}