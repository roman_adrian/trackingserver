<?php

namespace AppBundle\Service;

use AppBundle\Entity\TrackingOrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class FetchTrackingRequestFromDB implements FetchTrackingRequestInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Autowire the doctrine
     *
     * @param EntityManagerInterface $doctrine
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $doctrine, LoggerInterface $logger)
    {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
    }

    /**
     * fetch a tracking order from the database
     *
     * @inheritDoc
     */
    public function getTrackingRequest(string $trackingRequest): ?TrackingOrderEntity
    {
        /** @var TrackingOrderEntity $data */
        $data = $this->doctrine->getRepository('AppBundle:TrackingOrderEntity')->find($trackingRequest);
        if (!$data){
            //we did not find the records.
            $this->logger->warning('The search with the id ' . $trackingRequest . 'has return empty in the DB');
        }
        return $data;
    }
}