<?php
namespace AppBundle\Service;

use AppBundle\Entity\TrackingOrderEntity;

interface FetchTrackingRequestInterface
{
    /**
     * fetch a tracking order
     *
     * @param string $trackingRequest
     *
     * @return TrackingOrderEntity|Null
     */
    public function getTrackingRequest (string $trackingRequest):?TrackingOrderEntity;
}