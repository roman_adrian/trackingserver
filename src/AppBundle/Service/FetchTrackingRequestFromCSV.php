<?php

namespace AppBundle\Service;

use AppBundle\Entity\TrackingOrderEntity;
use Psr\Log\LoggerInterface;

class FetchTrackingRequestFromCSV implements FetchTrackingRequestInterface
{
    /**
     * @var string
     */
    private $pathToCSV;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string
     * @param LoggerInterface $logger
     */
    public function __construct(string $pathToCSV, LoggerInterface $logger)
    {
        $this->pathToCSV = $pathToCSV;
        $this->logger    = $logger;
    }

    /**
     * fetch a tracking order from the database
     *
     * @inheritDoc
     *
     */
    public function getTrackingRequest(string $trackingRequest): ?TrackingOrderEntity
    {
        try {
            /** @var resource $handle */
            if (($handle = fopen($this->pathToCSV, "r")) !== false) {
                /**
                 * $data contains a line from csv
                 *
                 * @var array
                 */
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    if ($data[0] == $trackingRequest) {
                        $order = new TrackingOrderEntity();
                        $order->setShippingDate(new \DateTime($data[1]));
                        $order->setTrackingId($data[0]);
                        fclose($handle);
                        return $order;
                    }
                }
                fclose($handle);
            } else {
                //the file could not be opened
                $this->logger->error('The csv specified in the parameters at the path: ' . $this->pathToCSV . ' failed to open');
            }
            //we did not find the records.
            $this->logger->warning('The search with the id ' . $trackingRequest . 'has return empty in the CSV');
        } catch (\Exception $e){
            //the file does not exist. We should log the error
            $this->logger->error('The csv specified in the parameters at the path: ' . $this->pathToCSV . ' does not exist');
        }
        return null;
    }
}